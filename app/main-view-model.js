const Observable = require("@nativescript/core").Observable;

function getMessage(counter) {
    if (counter <= 0) {
        return "You're driving too much! Come on, the polar bears are like...dying";
    } else {
        return 'Tap the button to get started on a summary of your fuel consumption';
    }
}
function getButtonLab(counter) {
    if (counter <= 0) {
        return "Re-run Analysis";
    } else {
        return 'Run Analysis';
    }
}
function getPageTitle(counter) {
    if (counter <= 0) {
        return "Analysis Complete";
    } else {
        return 'Welcome';
    }
}

function createViewModel() {
    const viewModel = new Observable();
    viewModel.counter = 1;
    viewModel.message = getMessage(viewModel.counter);
    viewModel.button_lab = getButtonLab(viewModel.counter);
    viewModel.page_title = getPageTitle(viewModel.counter);

    viewModel.onTap = () => {
        viewModel.counter--;
        viewModel.set("message", getMessage(viewModel.counter));
        viewModel.set("button_lab", getButtonLab(viewModel.counter));
        viewModel.set("page_title", getPageTitle(viewModel.counter));
    };

    return viewModel;
}

exports.createViewModel = createViewModel;
